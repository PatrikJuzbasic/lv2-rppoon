﻿using System;

namespace Kockice1


    {
        class Program
        {
            static void Main(string[] args)
            {
                DiceRoller dices = new DiceRoller();

                
                for (int i = 0; i < 20; i++)
                {
                    Console.Write("Unesi broj stranica: ");
                    string StringNumber = Console.ReadLine();
                    int IntNumber;
                    int.TryParse(StringNumber, out IntNumber);
                    Die die = new Die(IntNumber);
                    dices.InsertDie(die);
                }
                dices.RollAllDices();
                foreach (int Number in dices.GetRollingResults())
                {
                    Console.Write(" " + Number.ToString());
                }
                Console.Write("\n");

                
                DiceRoller dices2 = new DiceRoller();
                Random randomGenerator = new Random();
                for (int i = 0; i < 20; i++)
                {
                    Console.Write("Unesi broj stranica: ");
                    string StringNumber = Console.ReadLine();
                    int IntNumber;
                    int.TryParse(StringNumber, out IntNumber);
                    Die die = new Die(IntNumber, randomGenerator);
                    dices2.InsertDie(die);
                }
                dices2.RollAllDices();
                foreach (int Number in dices2.GetRollingResults())
                {
                    Console.Write(" " + Number.ToString());
                }
                Console.Write("\n");


            }
        }
    }


